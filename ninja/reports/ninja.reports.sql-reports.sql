USE `reports`;

DROP TABLE IF EXISTS `report_subscriptions`;

CREATE TABLE `report_subscriptions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cron_exp` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `next_trigger_time` timestamp NULL DEFAULT NULL,
  `report_type_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipper_id` int(10) DEFAULT NULL,
  `report_request_data` text COLLATE utf8mb4_unicode_ci,
  `report_format` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'csv',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipper_id_index` (`shipper_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `chunk_audit_logs`;

CREATE TABLE `chunk_audit_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_query_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_order_id` int(10) unsigned NOT NULL,
  `to_order_id` int(10) DEFAULT NULL,
  `number_of_tries` int(2) DEFAULT NULL,
  `seq_number` int(10) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_trigger_time` timestamp NULL DEFAULT NULL,
  `report_subscription_id` int(10) unsigned NOT NULL,
  `failure_reason` longblob,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_final_chunk` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_subscription_id_index` (`report_subscription_id`),
  CONSTRAINT `chunk_audit_logs_fk_1` FOREIGN KEY (`report_subscription_id`) REFERENCES `report_subscriptions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `report_audit_log`;

CREATE TABLE `report_audit_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `report_trigger_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `trigger_time` timestamp NULL DEFAULT NULL,
  `is_started` tinyint(1) DEFAULT '0',
  `is_completed` tinyint(1) DEFAULT '0',
  `nonce` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `report_audit_logs`;

CREATE TABLE `report_audit_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_chunk_count` int(11) DEFAULT NULL,
  `report_trigger_time` timestamp NULL DEFAULT NULL,
  `report_subscription_id` int(10) unsigned NOT NULL,
  `failure_reason` longblob,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_subscription_id_index` (`report_subscription_id`),
  CONSTRAINT `report_audit_logs_fk_1` FOREIGN KEY (`report_subscription_id`) REFERENCES `report_subscriptions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `report_email_recipients`;

CREATE TABLE `report_email_recipients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report_subscription_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_subscription_id_index` (`report_subscription_id`),
  CONSTRAINT `report_email_recipients_fk_1` FOREIGN KEY (`report_subscription_id`) REFERENCES `report_subscriptions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `report_triggers`;

CREATE TABLE `report_triggers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `trigger_code` int(11) DEFAULT NULL,
  `remarks` text,
  `emails` text,
  `template` text,
  `country` varchar(2) DEFAULT 'SG',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `schema_version`;

CREATE TABLE `schema_version` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `upload_failed_reports`;

CREATE TABLE `upload_failed_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `report_subscription_id` int(10) unsigned NOT NULL,
  `report_trigger_time` timestamp NULL DEFAULT NULL,
  `file_name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` longblob,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `report_subscription_id_index` (`report_subscription_id`),
  CONSTRAINT `upload_failed_reports_fk_1` FOREIGN KEY (`report_subscription_id`) REFERENCES `report_subscriptions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
