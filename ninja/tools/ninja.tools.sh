#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
export NAME="ninja.tools"

# change this!!
export FILE_LOCATION="/Users/felix.soewito/scripts/docker-compose/ninja/tools"
export DB_USERNAME="felix"
export DB_PASS="spMYtbidBKPaAV6BVxG0O9IfBzdMUtwA"

# get ip of the machine
export ip=$(ifconfig en0 | grep "inet " | awk '{print $2}')

MODE="start"
if [ "$#" -gt 0 ]
then
  MODE=$1
else
  echo "warn: no args called, fallback to $MODE"
fi

do_extra()
{
  echo "performing extra configurations"
  echo -e "${GREEN}have nothing: ${NC}"
  sleep 2s
}

start()
{
  echo -e "${GREEN}--> your ip is: ${ip}${NC}"
  docker-compose -f ${FILE_LOCATION}/${NAME}.dockercompose.yml up -d

  echo -e "starting the containers ........."
  echo -e "${GREEN}....... waiting for container to be started${NC}"
  sleep 5s

  echo -e "${GREEN}--> exporting docker-container-ids to ${NAME}${NC}"
  docker-compose -f ${FILE_LOCATION}/${NAME}.dockercompose.yml ps -q >> ${FILE_LOCATION}/${NAME}.pid
}

stop()
{
  FILE="${FILE_LOCATION}/${NAME}.pid"
  if test -f "$FILE"; then
    echo -e "${GREEN}--> ${FILE} exist!!${NC}"
    for WORD in `cat ${FILE}`
    do
        echo -e "${RED}Stopping ${WORD}${NC}"
        docker rm -f ${WORD}
    done
    echo -e "${GREEN}removing file ${FILE}${NC}"
    rm -f $FILE
  else 
    echo -e "${RED}--> ${FILE} does not exist${NC}"
  fi
}

if [ "$MODE" = "start" ]
then
  start
  do_extra
else
  stop
fi

echo "==============container status==============="
docker ps -a