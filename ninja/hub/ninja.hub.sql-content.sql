USE `hub_local_gl`;

INSERT INTO `hub_relations` (
  `origin_hub_id`, `origin_hub_system_id`, `origin_hub_type`,
  `destination_hub_id`, `destination_hub_system_id`, `destination_hub_type`,
  `parent_crossdock_hub_id`, `parent_crossdock_hub_system_id`,
  `number_of_transits`, `user_id`,
  `created_at`
)
VALUES
  (
    1, "sg", "station",
    2, "sg", "station",
    101, "sg",
    2, -1,
    NOW()
  ),
  (
    2, "sg", "station",
    101, "sg", "crossdock",
    101, "sg",
    1, -1,
    NOW()
  );

INSERT INTO `hub_relations_schedules` (
  `hub_relation_id`, `movement_type`,
  `day`, `start_time`, `duration`, `user_id`,
  `created_at`
) 
VALUES (
  1, "LANDHAUL",
  1, "2019-08-30 04:00:00", "01:04:30", -1,
  NOW()
);
