-- Database: hub_qa_gl
-- Generation Time: 2019-08-27 03:04:30 +0000
-- ************************************************************

USE hub_local_gl;

CREATE TABLE `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `actor` varchar(255) NOT NULL,
  `actor_id` int(11) NOT NULL,
  `actor_system_id` varchar(10) NOT NULL DEFAULT 'global',
  `entity` varchar(255) NOT NULL,
  `entity_id` int(11) NOT NULL,
  `entity_system_id` varchar(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=700 DEFAULT CHARSET=utf8;

CREATE TABLE `filter_templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `entity` varchar(50) NOT NULL,
  `payload` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=522 DEFAULT CHARSET=utf8;

CREATE TABLE `hub_route_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `system_id` varchar(10) NOT NULL,
  `hub_id` int(11) NOT NULL,
  `route_group_id` int(11) NOT NULL,
  `deleted_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_hub_route_group` (`system_id`,`hub_id`,`route_group_id`,`deleted_id`),
  KEY `hub_id` (`hub_id`,`system_id`),
  KEY `system_id` (`system_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4912 DEFAULT CHARSET=utf8;

CREATE TABLE `hubs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `linehaul_legs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `linehaul_id` int(11) NOT NULL,
  `orig_hub_country` varchar(10) NOT NULL DEFAULT '',
  `orig_hub_id` int(11) NOT NULL,
  `dest_hub_country` varchar(10) NOT NULL DEFAULT '',
  `dest_hub_id` int(11) NOT NULL,
  `seq_no` int(11) NOT NULL,
  `from_datetime_offset` bigint(20) NOT NULL,
  `to_datetime_offset` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14646 DEFAULT CHARSET=utf8;

CREATE TABLE `linehaul_transit_shipments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `linehaul_transit_id` int(11) DEFAULT NULL,
  `shipment_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `linehaul_transits` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `route_id` int(11) DEFAULT NULL,
  `waypoint_id` int(11) DEFAULT NULL,
  `linehaul_leg_id` int(11) DEFAULT NULL,
  `estimated_departure_time` timestamp NULL DEFAULT NULL,
  `estimated_arrival_time` timestamp NULL DEFAULT NULL,
  `actual_departure_time` timestamp NULL DEFAULT NULL,
  `actual_arrival_time` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `linehauls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `days_of_week` int(11) unsigned NOT NULL,
  `frequency` varchar(20) NOT NULL DEFAULT '',
  `comments` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6528 DEFAULT CHARSET=utf8;

CREATE TABLE `parameters` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `system_id` varchar(10) NOT NULL,
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(255) DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parameters_system_id` (`system_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

CREATE TABLE `scans` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `hub_country` varchar(10) NOT NULL DEFAULT '',
  `hub_id` int(11) NOT NULL,
  `route_id` int(11) DEFAULT NULL,
  `shipment_id` int(11) unsigned NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `waypoint_id` int(11) DEFAULT NULL,
  `scan_value` varchar(255) NOT NULL DEFAULT '',
  `source` varchar(255) NOT NULL,
  `result` varchar(255) DEFAULT NULL,
  `scan_type` varchar(255) NOT NULL,
  `from_entity` varchar(50) DEFAULT NULL,
  `to_entity` varchar(50) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_id` (`shipment_id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107192 DEFAULT CHARSET=utf8;

CREATE TABLE `shipment_events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) unsigned NOT NULL,
  `event` enum('UNKNOWN','SHIPMENT_CREATED','SHIPMENT_CLOSED','SHIPMENT_REOPENED','SHIPMENT_VAN_INBOUND','SHIPMENT_HUB_INBOUND','SHIPMENT_FORCE_COMPLETED','SHIPMENT_CANCELLED') NOT NULL DEFAULT 'UNKNOWN',
  `user_id` varchar(255) DEFAULT NULL,
  `hub_system_id` varchar(10) DEFAULT NULL,
  `hub_id` int(11) DEFAULT NULL,
  `status` enum('UNKNOWN','PENDING','CLOSED','TRANSIT','CANCELLED','COMPLETED') NOT NULL DEFAULT 'UNKNOWN',
  `scan_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_id` (`shipment_id`),
  KEY `event` (`event`),
  KEY `shipment_events_scans_id_fk` (`scan_id`),
  CONSTRAINT `shipment_events_scans_id_fk` FOREIGN KEY (`scan_id`) REFERENCES `scans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18772 DEFAULT CHARSET=utf8;

CREATE TABLE `shipment_events_copy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) unsigned NOT NULL,
  `event` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `hub_system_id` varchar(10) DEFAULT NULL,
  `hub_id` int(11) DEFAULT NULL,
  `status` varchar(11) DEFAULT NULL,
  `scan_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_id` (`shipment_id`),
  KEY `event` (`event`),
  KEY `shipment_events_scans_id_fk` (`scan_id`),
  CONSTRAINT `shipment_events_copy_ibfk_1` FOREIGN KEY (`scan_id`) REFERENCES `scans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `shipment_ext_awbs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_id` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ref_id` (`ref_id`)
) ENGINE=InnoDB AUTO_INCREMENT=506 DEFAULT CHARSET=utf8;

CREATE TABLE `shipment_orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `shipment_id` int(11) NOT NULL,
  `order_country` varchar(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipment_id` (`shipment_id`),
  KEY `order_country` (`order_country`,`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43318 DEFAULT CHARSET=utf8;

CREATE TABLE `shipments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) NOT NULL DEFAULT '',
  `user_id` varchar(255) DEFAULT NULL,
  `shipment_ext_awb_id` bigint(20) unsigned DEFAULT NULL,
  `orig_hub_country` varchar(10) NOT NULL DEFAULT '',
  `orig_hub_id` int(11) NOT NULL,
  `dest_hub_country` varchar(10) NOT NULL DEFAULT '',
  `dest_hub_id` int(11) NOT NULL,
  `curr_hub_country` varchar(10) NOT NULL DEFAULT '',
  `curr_hub_id` int(11) NOT NULL,
  `shipment_type` varchar(32) NOT NULL DEFAULT 'OTHERS',
  `status` varchar(11) NOT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `arrival_datetime` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `comments` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uuid` (`uuid`),
  KEY `orig_hub_country` (`orig_hub_country`),
  KEY `dest_hub_country` (`dest_hub_country`),
  KEY `curr_hub_country` (`curr_hub_country`),
  KEY `fk_shipment_ext_awbs` (`shipment_ext_awb_id`),
  KEY `shipment_type` (`shipment_type`),
  KEY `status` (`status`),
  KEY `created_at` (`created_at`),
  CONSTRAINT `shipments_ibfk_1` FOREIGN KEY (`shipment_ext_awb_id`) REFERENCES `shipment_ext_awbs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51382 DEFAULT CHARSET=utf8;
