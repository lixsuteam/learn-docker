#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

export NAME='hub-service'
export FILE_LOCATION=$(pwd)
export DB_USERNAME=$NINJA_DB_USERNAME
export PORT_HZ="5701"
export PORT_KAFKA="9092"
if [ -z "$NINJA_DB_PASSWORD" ]
then
      export DB_PASS="undefined"
else
      export DB_PASS=$NINJA_DB_PASSWORD
fi

# get ip of the machine
export IP=$(ifconfig en0 | grep "inet " | awk '{print $2}')

MODE="start"
if [ "$#" -gt 0 ]
then
  MODE=$1
else
  echo "warn: no args called, fallback to $MODE"
fi

do_extra()
{
  echo "performing extra configurations"
  echo -e "${GREEN}creating kafka topics: ${NC}"
  docker run --rm ches/kafka kafka-topics.sh --create --topic exception-event-dev-topic --replication-factor 1 --partitions 1 --zookeeper $IP:2181
  echo -e "${GREEN}have nothing ${NC}"
}

L_RETVAL="??????????"
mask()
{
  local n=4                    # number of chars to leave
  local a="${1:0:${#1}-n}"     # take all but the last n chars
  local b="${1:${#1}-n}"       # take the final n chars
  L_RETVAL="${a//?/*}$b"
}

start()
{
  mask ${NINJA_DB_PASSWORD}
  local MASKED_PASS=$L_RETVAL
  echo -e "${GREEN}--> your ip is: ${IP}${NC}"
  echo -e "${GREEN}--> your file location: ${FILE_LOCATION}${NC}"
  echo -e "${YELLOW}--> your db creds: ${DB_USERNAME}:${MASKED_PASS}${NC}"
  docker-compose -f ${FILE_LOCATION}/ninja.hub.dockercompose.yml up -d

  echo -e "starting the containers ........."
  echo -e "${GREEN}....... waiting for container to be started${NC}"
  sleep 5s

  echo -e "${GREEN}--> exporting docker-container-ids to ${NAME}.pid${NC}"
  docker-compose -f ${FILE_LOCATION}/ninja.hub.dockercompose.yml ps -q >> ${FILE_LOCATION}/${NAME}.pid
}

stop()
{
  FILE="${FILE_LOCATION}/${NAME}.pid"
  if test -f "$FILE"; then
    echo -e "${GREEN}--> ${FILE} exist!!${NC}"
    for WORD in `cat ${FILE}`
    do
        echo -e "${RED}Stopping ${WORD}${NC}"
        docker rm -f ${WORD}
    done
    echo -e "${GREEN}removing file ${FILE}${NC}"
    rm -f $FILE
  else 
    echo -e "${RED}--> ${FILE} does not exist${NC}"
  fi
}

if [ "$MODE" = "start" ]
then
  start
  do_extra
else
  stop
fi

echo "==============container status==============="
docker ps -a