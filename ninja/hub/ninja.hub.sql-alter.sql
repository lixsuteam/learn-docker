USE `hub_local_gl`;

ALTER TABLE `shipments` 
  ADD `sla` timestamp NULL DEFAULT NULL;

CREATE TABLE `hub_relations` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

  `origin_hub_id` int(11) NOT NULL,
  `origin_hub_system_id` varchar(10) NOT NULL,

  `destination_hub_id` int(11) NOT NULL,
  `destination_hub_system_id` varchar(10) NOT NULL,

  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  CONSTRAINT `hub_relations_id_pk` PRIMARY KEY (`id`),
  INDEX `hub_relations_origin_hub_id_index` (`origin_hub_id`),
  INDEX `hub_relations_origin_hub_system_id_index` (`origin_hub_system_id`),
  INDEX `hub_relations_destination_hub_id_index` (`destination_hub_id`),
  INDEX `hub_relations_destination_hub_system_id_index` (`destination_hub_system_id`),
  INDEX `hub_relations_deleted_at_index` (`deleted_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hub_crossdock_details` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `station_hub_id` INT(11) NOT NULL,
  `station_hub_system_id` varchar(10) NOT NULL,
  `crossdock_hub_id` int(11) NOT NULL,
  `crossdock_hub_system_id` varchar(10) NOT NULL,
  `number_of_transits` int(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  CONSTRAINT `hub_crossdock_details_id_pk` PRIMARY KEY (`id`),
  INDEX `hub_crossdock_details_crossdock_hub_id_index` (`crossdock_hub_id`),
  INDEX `hub_crossdock_details_crossdock_hub_system_id_index` (`crossdock_hub_system_id`),
  INDEX `hub_crossdock_details_deleted_at_index` (`deleted_at`),
  INDEX `hub_crossdock_details_station_hub_id_index` (`station_hub_id`),
  INDEX `hub_crossdock_details_station_hub_system_id_index` (`station_hub_system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `hub_relation_schedules` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hub_relation_id` INT(11) UNSIGNED NOT NULL,
  `movement_type` ENUM("LAND_HAUL", "AIR_HAUL", "SEA_HAUL", "OTHER") NOT NULL,
  `movement_name` VARCHAR(255) NOT NULL,
  `day` INT(2) NOT NULL,
  `start_time` TIME NOT NULL,
  `duration` VARCHAR(10) NOT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  CONSTRAINT `hub_relation_schedules_id_pk` PRIMARY KEY (`id`),
  INDEX `hub_relation_schedules_movement_type_index` (`movement_type`),
  INDEX `hub_relation_schedules_deleted_at_index` (`deleted_at`),
  INDEX `hub_relation_schedules_hub_relation_id_index` (`hub_relation_id`),
  CONSTRAINT `hub_relation_schedules_hub_relations_fk` FOREIGN KEY (`hub_relation_id`) REFERENCES `hub_relations` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE `movement_events` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `shipment_id` INT(11) UNSIGNED NOT NULL,
  `event` VARCHAR(30) NOT NULL,
  `status` ENUM("SUCCESS", "FAILED") NOT NULL,
  `ext_data` TEXT NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT `movement_events_id_pk` PRIMARY KEY (`id`),
  INDEX `movement_events_shipment_id_index` (`shipment_id`),
  CONSTRAINT `movement_events_shipments_fk` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;
