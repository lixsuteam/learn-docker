USE `hackday`

CREATE TABLE `claimed_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `tracking_id` varchar(36) NOT NULL DEFAULT '',
  `shipper_id` int(11) NOT NULL,
  `status` varchar(11) NOT NULL,
  `assignee` varchar(64) DEFAULT NULL,
  `liable_dept` varchar(24),
  `amount` decimal(20,4) DEFAULT NULL,
  `paid_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipper_id` (`shipper_id`),
  KEY `tracking_id` (`tracking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;