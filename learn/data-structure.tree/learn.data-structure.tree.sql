CREATE DATABASE `my-tree`;
USE `my-tree`;
CREATE TABLE `nodes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `lft` int(11) unsigned DEFAULT NULL,
  `rgt` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO nodes (name, lft, rgt, created_at)
VALUES
('ELECTRONICS',1,20, NOW()),
('TELEVISIONS',2,9, NOW()),
('TUBE',3,4, NOW()),
('LCD',5,6, NOW()),
('PLASMA',7,8, NOW()),
('PORTABLE ELECTRONICS',10,19, NOW()),
('MP3 PLAYERS',11,14, NOW()),
('FLASH',12,13, NOW()),
('CD PLAYERS',15,16, NOW()),
('2 WAY RADIOS',17,18, NOW());