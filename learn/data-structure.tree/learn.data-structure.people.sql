CREATE DATABASE `common`;
USE `common`;
CREATE TABLE `people` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `type` tinyint(2) DEFAULT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO people (name, age, type, created_at)
VALUES
('Felix',20, 1, NOW()),
('Dini',18, 2, NOW()),
('Ason',19, 3,NOW()),
('Joi',25, 1, NOW()),
('Lixsu',22, 0, NOW());