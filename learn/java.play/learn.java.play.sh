#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
export NAME="learn.java.play"

# change this!!
export FILE_LOCATION="/Users/felix.soewito/scripts/docker-compose/learn/java.play"
export DB_USERNAME="user"
export DB_PASS="password"

# get ip of the machine
export ip=$(ifconfig en0 | grep "inet " | awk '{print $2}')

MODE="start"
if [ "$#" -gt 0 ]
then
  MODE=$1
else
  echo "warn: no args called, fallback to $MODE"
fi

start()
{
  echo -e "${GREEN}--> your ip is: ${ip}${NC}"
  docker-compose -f ${FILE_LOCATION}/learn.java.play.dockercompose.yml up -d

  echo -e "starting the containers ........."
  echo -e "${GREEN}....... waiting for container to be started${NC}"
  sleep 5s

  echo -e "${GREEN}--> exporting docker-container-ids to ${NAME}${NC}"
  docker-compose -f ${FILE_LOCATION}/learn.java.play.dockercompose.yml ps -q >> ${FILE_LOCATION}/${NAME}.pid
}

stop()
{
  FILE="${FILE_LOCATION}/${NAME}.pid"
  if test -f "$FILE"; then
    echo -e "${GREEN}--> ${FILE} exist!!${NC}"
    for WORD in `cat ${FILE}`
    do
        echo -e "${RED}Stopping ${WORD}${NC}"
        docker rm -f ${WORD}
    done
    echo -e "${GREEN}removing file ${FILE}${NC}"
    rm -f $FILE
  else 
    echo -e "${RED}--> ${FILE} does not exist${NC}"
  fi
}

do_extra()
{
  echo "performing extra configurations"
  echo -e "${GREEN}creating kafka topics: ${NC}"
  docker run --rm ches/kafka kafka-topics.sh --create --zookeeper $ip:2181 --replication-factor 1 --partitions 3 --topic demo-1
  docker run --rm ches/kafka kafka-topics.sh --create --zookeeper $ip:2181 --replication-factor 1 --partitions 3 --topic demo-2

  echo -e "${GREEN}topics available:${NC}"
  docker run --rm ches/kafka kafka-topics.sh --list --zookeeper $ip:2181
  sleep 2s

  echo -e "to publish a message run: "
  echo -e "docker run --rm -i -t ches/kafka kafka-console-producer.sh --topic demo-1 --broker-list $ip:9092"
  echo -e "to consume a message run:"
  echo -e "docker run --rm -i -t ches/kafka kafka-console-consumer.sh --topic demo-1 --from-beginning --bootstrap-server $ip:9092"
  echo -e "to delete topic"
  echo -e "docker run --rm ches/kafka kafka-topics.sh --zookeeper $ip:2181 --delete --topic demo-1"
  echo -e "to describe topic"
  echo -e "docker run --rm ches/kafka kafka-topics.sh --zookeeper $ip:2181 --describe --topic demo-1"
}

if [ "$MODE" = "start" ]
then
  start
  do_extra
else
  stop
fi

echo -e "==============container status==============="
echo -e "container status disabled"
#docker ps -a