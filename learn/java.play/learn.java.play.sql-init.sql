USE `learn-java-play`;

CREATE TABLE `people` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `age` int(4) unsigned NOT NULL,
  `system_id` varchar(8) NOT NULL DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO `people` (`name`, `age`, `system_id`, `created_at`)
VALUES ("felix", 20, "ID", NOW()), ("clyde", 21, "ID", NOW()), ("biscuit", 23, "ID", NOW());
