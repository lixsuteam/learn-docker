#!/bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
export NAME="learn.service.kafka"

# change this!!
export FILE_LOCATION="/Users/felix.soewito/scripts/docker-compose/learn/service.kafka"

# get ip of the machine
export IP=$(ifconfig en0 | grep "inet " | awk '{print $2}')
export PORT1="9092"
export PORT2="9093"

MODE="start"
if [ "$#" -gt 0 ]
then
  MODE=$1
else
  echo "warn: no args called, fallback to $MODE"
fi

start()
{
  echo -e "${GREEN}--> your ip is: ${IP}${NC}"
  docker-compose -f ${FILE_LOCATION}/${NAME}.dockercompose.yml up -d

  echo -e "starting the containers ........."
  echo -e "${GREEN}....... waiting for container to be started${NC}"
  sleep 5s

  echo -e "${GREEN}--> exporting docker-container-ids to ${NAME}${NC}"
  docker-compose -f ${FILE_LOCATION}/${NAME}.dockercompose.yml ps -q >> ${FILE_LOCATION}/${NAME}.pid
}

stop()
{
  FILE="${FILE_LOCATION}/${NAME}.pid"
  if test -f "$FILE"; then
    echo -e "${GREEN}--> ${FILE} exist!!${NC}"
    for WORD in `cat ${FILE}`
    do
        echo -e "${RED}Stopping ${WORD}${NC}"
        docker rm -f ${WORD}
    done
    echo -e "${GREEN}removing file ${FILE}${NC}"
    rm -f $FILE
  else 
    echo -e "${RED}--> ${FILE} does not exist${NC}"
  fi
}

do_extra()
{
  C1=`docker ps | grep kafka | awk NR==1'{print $1}'`
  C2=`docker ps | grep kafka | awk NR==2'{print $1}'`
  echo "performing extra configurations"
  echo "service 1 up and ready at $IP:$PORT1 on $C1"
  echo "service 2 up and ready at $IP:$PORT2 on $C2"
  echo -e "${GREEN}creating kafka topics: ${NC}"
  docker run --name kafka-dummy --rm ches/kafka kafka-topics.sh --create --topic hello-topic --replication-factor 2 --partitions 3 --zookeeper $IP:2181
  docker run --name kafka-dummy --rm ches/kafka kafka-topics.sh --list --zookeeper $IP:2181
}

if [ "$MODE" = "start" ]
then
  start
  do_extra
else
  stop
fi
