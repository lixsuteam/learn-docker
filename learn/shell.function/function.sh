#!/bin/bash
NAME="learn.shell.function"

echo "running shell script: $NAME"

MODE="start"
if [ "$#" -gt 0 ]
then
  MODE=$1
else
  echo "warn: no args called, fallback to $MODE"
fi

start()
{
  echo "start is called"
}

stop() {
  echo "stop is called"
}

echo "mode: $MODE"
echo "running the function...."
if [ "$MODE" = "start" ]
then
  start
else
  stop
fi
